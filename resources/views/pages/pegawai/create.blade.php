@extends('layouts.master')

@section('title', 'Dashboard')
@section('title-page', 'Dashboard')
@section('content')

    <div class="row">
        <div class="col-md-8">
            <a href="{{ route('pegawai') }}" class="btn btn-primary">Kembali</a>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <form action="{{ route('pegawai.store') }}" style="margin-top: 30px; margin-bottom: 30px;" method="post" onsubmit="return confirm('Tambah Data?'); ">

        @csrf

        <div class="form-group {{ $errors->first('nama') ? 'has-error' : '' }}">
            <label for="nama">Nama</label>
            <input type="text" name="nama" class="form-control" placeholder="Masukan Nama">

            <div class="text-danger">
                {{ $errors->first('nama') }}
            </div>
        </div>

        <div class="form-group {{ $errors->first('alamat') ? 'has-error' : '' }}">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10"></textarea>

            <div class="text-danger">
                {{ $errors->first('alamat') }}
            </div>
        </div>

        <div class="form-footer">
            <button type="submit" class="btn btn-info text-white">Tambah</button>
        </div>

    </form>

@endsection

