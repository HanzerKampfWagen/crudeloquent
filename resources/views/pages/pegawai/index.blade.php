@extends('layouts.master')

@section('title', 'Dashboard')
@section('title-page', 'Dashboard')
@section('content')

    <div class="row mb-4">
        <div class="col-md-8">
            <a href="{{ route('pegawai.create') }}" class="btn btn-primary">Tambah Data</a>
        </div>

        <div class="col-md-4">
            <form action="{{ route('pegawai.search') }}" method="get">
                <input type="search" class="form-control" name="keyword" placeholder="Cari Sesuai Nama">
            </form>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <table class="table table-hover">
        <thead class="thead-light text-center">
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Opsi</th>
            </tr>
        </thead>

        @php $no = 1 @endphp
        @foreach( $pegawai as $p )
        <tbody class="text-center">
            <tr>
                <td>{{ $no }}.</td>
                <td>{{ $p->id }}</td>
                <td><a href="{{ route('pegawai.edit', $p->id) }}">{{ $p->nama }}</a></td>
                <td>{{ $p->alamat }}</td>
                <td>
                    <form action="{{ route('pegawai.destroy', $p->id) }}" method="post" onsubmit="return confirm('Hapus Data {{ $p->nama }} ?')">
                        @csrf
                        @method("DELETE")
                        <button type="submit" class="btn btn-outline-danger">Hapus</button>
                    </form>
                </td>
            </tr>
        </tbody>
        @php $no++ @endphp
        @endforeach

    </table>

    {{ $pegawai->links() }}
@endsection

